using PianoViaR.Piano.Behaviours.Keys;
using UnityEngine;

public class PianoKeysHolder : MonoBehaviour
{
    public PianoKey[] Keys;
}